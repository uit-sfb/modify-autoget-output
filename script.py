import sys, argparse, os
import numpy as np
import pandas as pd

def main(argv):
   parser = argparse.ArgumentParser(description='')
   parser.add_argument('-i', dest='input', default='', help='autoget data')
   parser.add_argument('-m', dest='mapping', default='', help='mapping table')
   parser.add_argument('-o', dest='output', default='', help='output directory')
   parser.add_argument('--mmp_ids', dest='mmp', default='', help='table with mmp ids')
   args = parser.parse_args()
   return args.input, args.mapping, args.output, args.mmp;

def writeToTsv(df,outPath):
    if not df.empty:
        df.to_csv(outPath,sep='\t',index=False)

def convertToDataFrame(tsv):
    df = pd.read_csv(tsv,sep='\t',engine='python',index_col=False,thousands=',')
    df.columns = df.columns.str.strip()
    return df

def mapValues(autoget, mapping_table, output, mmp_ids):
    autoget_df = convertToDataFrame(autoget)
    mapping_df = convertToDataFrame(mapping_table)
    mmp_df = convertToDataFrame(mmp_ids)
    #rename first unnamed column
    autoget_df.rename(columns={'Unnamed: 0':'id'}, inplace=True)

    mmp_df = mmp_df.dropna()

    for c in mmp_df.columns:
        mmp_df[c] = mmp_df[c].apply(lambda x: x.strip().split(":").pop())

    mmp_dict = mmp_df.set_index('asmbl:sequences')['id'].to_dict()
    mapping_dict = mapping_df.set_index('Autoget output')['Mar attribute'].to_dict()
    #substitute gca ids with mmp ids
    autoget_df.iloc[:, 0] = autoget_df.iloc[:, 0].apply(lambda x: x.replace(".fa","")).apply(lambda x: mmp_dict.get(x,x))
    #merge columns 5s, 16s and 23s together, np.rint is used to conevrt numeric values to Int64, we need to convert them to Int32 to remove .0 part, .dropna removes NaN values, convertion to string is needed to join values.
    autoget_df['5s'] = autoget_df[['5s','16s','23s']].apply(lambda x: ','.join(np.rint(x).dropna().astype(np.int32).astype(str)),axis=1)
    autoget_df.drop(['16s','23s'],axis=1,inplace=True)
    #set column names to lower case
    lowerColumns = list(map(lambda x: x.lower(),list(autoget_df.columns)))
    autoget_df.columns = lowerColumns
    #change column names
    autoget_df.columns = list(map(lambda x: mapping_dict.get(x,x),lowerColumns))
    #replace ; with >
    autoget_df['tax:gtdb_classification'] = autoget_df['tax:gtdb_classification'].apply(lambda x: str(x).replace(';','>') if str(x) != "nan" else x)

    writeToTsv(autoget_df,output)

if __name__ == "__main__":
   input, mapping, output, mmp_ids = main(sys.argv[1:])
   mapValues(input,mapping, output, mmp_ids)

