FROM python:3.9.5

WORKDIR /app/script

ENV HOME /app/script

RUN pip install --no-cache-dir pandas numpy

COPY *.py ./

ENTRYPOINT [ "python3", "./script.py" ]

